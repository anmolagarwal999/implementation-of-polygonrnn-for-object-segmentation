import torch
import torch.nn.init
import torch.utils.model_zoo as model_zoo
import wget
from torch import nn

from utils.convlstm import ConvLSTM



class PolygonRnn(nn.Module):

    def __init__(self):
            super(PolygonRnn, self).__init__()

        
            self.CHOSEN_KERNEL_SIZE=3
            self.PADDING_FOR_ALL_CONVOLUTIONS=1
            self.STRIDE_FOR_ALL_CONVOLUTIONS=1
            self.DIMENSION_OF_PREPROCESSED_IMAGE=128
            self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE=28

            # architecture: https://miro.medium.com/max/1400/1*NNifzsJ7tD2kAfBXt3AzEg.png
            # conv2d: (in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True, padding_mode='zeros', device=None, dtype=None)
            # Expected input: 224 * 224 * 3
            # Expected output: 56 * 56 * 128
            self.intermediate_layer_1 = nn.Sequential(
                nn.Conv2d(3, 64, 3, 1, 1),
                nn.BatchNorm2d(64),
                nn.ReLU(), #         224+2-3+1=224
                nn.Conv2d(64, 64, 3, 1, 1),
                nn.BatchNorm2d(64),
                nn.ReLU(),      #   224+2-3+1=224
                nn.MaxPool2d(2, 2), # 113
                nn.Conv2d(64, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1), 
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE),
                nn.ReLU(), # 112+2-3+1=112
                nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE),
                nn.ReLU(),
                nn.MaxPool2d(2, 2) # 112/2=56
            )
            self.skip_connection_conv_1 = nn.Sequential(
                nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
                nn.ReLU(),
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
            )
            #######################################################
            # Expected input: 56 * 56 * 128
            # Expected output: 28 * 28 * 256
            self.intermediate_layer_2 = nn.Sequential(
                nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, 256, 3, 1, 1), # 56 -3 + 2 + 1 =56
                nn.BatchNorm2d(256),
                nn.ReLU(),
                nn.Conv2d(256, 256, 3, 1, 1), # 56 -3 + 2 + 1 =56
                nn.BatchNorm2d(256),
                nn.ReLU(),
                nn.Conv2d(256, 256, 3, 1, 1),# 56 -3 + 2 + 1 =56
                nn.BatchNorm2d(256),
                nn.ReLU(),
                nn.MaxPool2d(2, 2) # 56/2=28
            )
            self.skip_connection_conv_2 = nn.Sequential(
                nn.Conv2d(256, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
                nn.ReLU(),
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
            )
            #######################################################
            # Expected input: 28 * 28 * 256
            # Expected output: 28 * 28 * 512
            self.intermediate_layer_3 = nn.Sequential(
                nn.Conv2d(256, 512, 3, 1, 1), # 28-3+2+1= 28
                nn.BatchNorm2d(512),
                nn.ReLU(),
                nn.Conv2d(512, 512, 3, 1, 1), # 28-3+2+1= 28
                nn.BatchNorm2d(512),
                nn.ReLU(),
                nn.Conv2d(512, 512, 3, 1, 1), # 28-3+2+1= 28
                nn.BatchNorm2d(512),
                nn.ReLU()
            )
            self.skip_connection_conv_3 = nn.Sequential(
                nn.Conv2d(512, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
                nn.ReLU(),
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
            )
            # Maxpool not included as we want the input to the skip connections to be 28 * 28. Applying max pool here would need skip connections layer to get an input of 14 * 14 from this branch.
            # paper does not mention anything about removing this max pool layer.
            # So, I shift the max pool layer to the next CONV chunk.
            #######################################################
            # Expected input: 28 * 28 * 512
            # Expected output: 14 * 14 * 512
            self.intermediate_layer_4 = nn.Sequential(
                nn.MaxPool2d(2, 2), # 28/2=14
                nn.Conv2d(512, 512, 3, 1, 1),
                nn.BatchNorm2d(512),
                nn.ReLU(),
                nn.Conv2d(512, 512, 3, 1, 1), # 28 -3+2+1=28
                nn.BatchNorm2d(512),
                nn.ReLU(),
                nn.Conv2d(512, 512, 3, 1, 1), # 28 -3+2+1=28
                nn.BatchNorm2d(512),
                nn.ReLU()
            )
            self.skip_connection_conv_4 = nn.Sequential(
                nn.Conv2d(512, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
                nn.ReLU(),
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
            )
            self.pool_for_skip_connections = nn.MaxPool2d(2, 2).to('cuda')
            # NOTE: paper mentions: . We first remove the fully connected layers as well as the last max-pooling layer, pool5.
            #######################################################

            self.conv_from_skip_connection_to_rnn = nn.Sequential(
                nn.Conv2d(512, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
                nn.ReLU(),
                nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
            )
            ################################
            # self.pool_for_skip_connections = nn.MaxPool2d(2, 2)
            ##################################

            ###############################################################
            # Authors mention mode = bilinear
            # Since we use features from multiple skiplayers which have different spatial dimensions, we employ bilinear upsampling or max-pooling in order to get outputs that all have the same spatial resolution. 

            self.upsample_layer = nn.Upsample(scale_factor=2, mode='bilinear').to('cuda')
            ######################################################

            self.conv_lstm_layer = ConvLSTM(cnn_embedding_size=(self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE),
                                    channels_in_input=131, # ie channels in x_t
                                    hidden_dim=[16, 16],
                                    num_layers=2,
                                    batch_first=True,
                                    kernel_size=(3, 3),
                                    bias=True,
                                    return_all_layers=True)


            self.lstm_layer = nn.LSTM(self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * 8 + (self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE + 1) * 2, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * 2,
                                    batch_first=True)
            self.linear = nn.Linear(self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * 2, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE + 1)


            self.vgg.load_state_dict('vgg16.pth')

            # self.v_conv = nn.Sequential(
            #     nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, 1, 3, padding='same'),
            #     nn.ReLU()
            # )
            # self.e_conv = nn.Sequential(
            #     nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, 1, 3, padding='same'),
            #     nn.ReLU()
            # )


    def fetch_max(self,v ):
        return v.max(dim=2, keepdim=True)[1][0][0][0].item()

    def fetch_flattened_coordinates(self, col  ,row):
        return row*self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE+col

    def fetch_2d(self, flattened_v):
        return (flattened_v%self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, flattened_v//self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)

    def fetch_manhattan(self, a, b):
        return abs(a[0]-b[0])+abs(a[1]+b[1])

    def forward(self, resized_img, ground_truth_first_one_hot, all_gt_vertices_one_hot, shifted_gt_vertices_one_hot):


        '''
        details:
        resized_image: image in 228 x 228 space
        ground_truth_first_one_hot = one hot encoding (flattened) of first vertex
        all_gt_vertices_one_hot: contains one hot encoding of all vertices for all timesteps
        shifted_gt_vertices_one_hot: shifted one hot encoding to feed previous vertex, shifted vertex version of "second"

        '''

        print("Shape of resized_img is ", resized_img.size())


        num_elems_in_batch = all_gt_vertices_one_hot.shape[0]
        print("num_elems_in_batch is ", num_elems_in_batch)


        max_vertices_allowed = all_gt_vertices_one_hot.shape[1]
        print("len s is ", max_vertices_allowed)

        # FIRST CONV BLOCK
        block_1_output = self.intermediate_layer_1(resized_img)
        print("Shape of block output 1 is ", block_1_output.size())
        assert(block_1_output.size()==torch.Size([num_elems_in_batch, self.DIMENSION_OF_PREPROCESSED_IMAGE, 56, 56]))


        ################################################################
        # SECOND CONV BLOCK
        block_2_output = self.intermediate_layer_2(block_1_output)
        print("Shape of block output 2 is ", block_2_output.size())
        assert(block_2_output.size()==torch.Size([num_elems_in_batch, 256, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))


        # THIRD CONV BLOCK
        output3 = self.intermediate_layer_3(block_2_output)
        print("Shape of block output 3 is ", output3.size())
        assert(output3.size()==torch.Size([num_elems_in_batch, 512, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

  
        ##############################################

        # 4th CONV BLOCK
        block_4_output = self.intermediate_layer_4(output3)
        print("Shape of block output 4 is ", block_4_output.size())
        assert(block_4_output.size()==torch.Size([num_elems_in_batch, 512, 14, 14]))

        ###############################################################
        # operations for skip connection 1
        input_to_skip_1 = self.pool_for_skip_connections(block_1_output)
        input_to_skip_1 = self.skip_connection_conv_1(input_to_skip_1)
        print("Shape of input_to_skip_1 is ", input_to_skip_1.size())
        assert(input_to_skip_1.size()==torch.Size([num_elems_in_batch, self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

        # operations for skip connection 2
        input_to_skip_2 = self.skip_connection_conv_2(block_2_output)
        print("Shape of input_to_skip_2 is ", input_to_skip_2.size())
        assert(input_to_skip_2.size()==torch.Size([num_elems_in_batch, self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

        # operations for skip connection 3
        input_to_skip_3 = self.skip_connection_conv_3(output3)
        print("Shape of input_to_skip_3 is ", input_to_skip_3.size())
        assert(input_to_skip_3.size()==torch.Size([num_elems_in_batch, self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

        # Operations for skip connection 4
        input_to_skip_4 = self.skip_connection_conv_4(block_4_output)
        print("Shape of input_to_skip_4 is ", input_to_skip_4.size())
        assert(input_to_skip_4.size()==torch.Size([num_elems_in_batch, self.DIMENSION_OF_PREPROCESSED_IMAGE, 14, 14]))
        input_to_skip_4 = self.upsample_layer(input_to_skip_4)
        print("Shape of input_to_skip_4 after upsamplong is ", input_to_skip_4.size())
        assert(input_to_skip_4.size()==torch.Size([num_elems_in_batch, self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

        # concatenting outputs from all CONV BLOCKS
        skip_connection_layer_output = torch.cat([input_to_skip_1, input_to_skip_2, input_to_skip_3, input_to_skip_4], dim=1)
        print("Shape of concatenated output is ", skip_connection_layer_output.size())
        assert(skip_connection_layer_output.size()==torch.Size([num_elems_in_batch, 512, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

        # Convolution on CNN features before inputting to LSTM
        nlp_output = self.conv_from_skip_connection_to_rnn(skip_connection_layer_output)
        print("Shape of concatenated output after layer 5 is ", nlp_output.size())
        assert(nlp_output.size()==torch.Size([num_elems_in_batch, 128, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]))

        #####################################################################
        # Duplicating the CONV features for all time steps
        output = nlp_output.unsqueeze(1)
        output = output.repeat(1, max_vertices_allowed, 1, 1, 1)
        ########################################################

        dummy_zero = torch.zeros([num_elems_in_batch, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE])

        # first vertex needs to be send to all timesteps
        first_vertex_all_timesteps = ground_truth_first_one_hot[:, :-3].view(-1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE).unsqueeze(1).repeat(1,
                                                                        max_vertices_allowed - 1,
                                                                        1, 1,
                                                                        1)

        print("Shape of first_vertex_all_timesteps is ", first_vertex_all_timesteps.size())


        first_vertex_all_timesteps = torch.cat([dummy_zero, first_vertex_all_timesteps], dim=1)
        print("Shape of first_vertex_all_timesteps next step is ", first_vertex_all_timesteps.size())


        
        gt_vertices = all_gt_vertices_one_hot[:, :, :-3].view(-1, max_vertices_allowed, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)
        print("Shape of gt_vertices is ", gt_vertices.size())

        shifted_gt_vertices = shifted_gt_vertices_one_hot[:, :, :-3].view(-1, max_vertices_allowed, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)
        print("Shape of shifted_gt_vertices is ", shifted_gt_vertices.size())

        output = torch.cat([output, first_vertex_all_timesteps, gt_vertices, shifted_gt_vertices], dim=2)
        print("Shape of output after cat is ", output.size())

        ######################################################################
        # conv lstm returns a tuple of (layer_output_list, last_state_list)
        # we are only concerned with the output of the last STACKED LSTM and so, [-1]
        output = self.conv_lstm_layer(output)[0][-1]
        print("Shape of output after conv lstm is ", output.size())
        ############################################################



        output_shape = output.shape
        # [2, 58, 8, 28, 28]
        print("output shape is ", output_shape)

        # When you call contiguous(), it actually makes a copy of the tensor such that the order of its elements in memory is the same as if it had been created from scratch with the same data.
        output = output.contiguous().view(num_elems_in_batch, max_vertices_allowed, -1)
        # [2, 58, 6272]
        print("Shape of output after contiguous is ", output.size())

        # [2, 58, 7846]
        output = torch.cat([output, all_gt_vertices_one_hot, shifted_gt_vertices_one_hot], dim=2)
        print("Shape of output after CAT is ", output.size())




        # [2, 58, 1568]
        output = self.lstm_layer(output)[0]
        print("Shape of output after LSTM LAYER is ", output.size())

        output = output.contiguous().view(num_elems_in_batch * max_vertices_allowed, -1)
        print("Shape of output after 2nd contig view is ", output.size())


        output = self.linear(output)
        print("Shape of output after LINEAR is ", output.size())

        output = output.contiguous().view(num_elems_in_batch, max_vertices_allowed, -1)
        # [2, 58, 787]
        print("Shape of output after 3rd contiguous view is ", output.size())
        return output



    def test(self, cnn_input, max_timesteps, gt_list, corrections_allowed, THRESHOLD):

        # print("shape of input data1 is ", cnn_input.shape)
        # print("len s is ", max_timesteps)
        batch_size = cnn_input.shape[0]
        # print("batch size is ", batch_size)

        # model_predictions stores the predicted flattened vertex coordinates of the image
        model_predictions = torch.zeros([batch_size, max_timesteps]).to('cuda')
        # model_predictions ZEROES SHAPE IS  torch.Size([1, 60])
        # model_predictions = torch.zeros([batch_size, max_timesteps])
        # print("model_predictions ZEROES SHAPE IS ", model_predictions.shape)

        

        # cnn skip connections stuff
        tmp1 = self.intermediate_layer_1(cnn_input)
        tmp2 = self.intermediate_layer_2(tmp1)
        tmp3 = self.intermediate_layer_3(tmp2)
        tmp4 = self.intermediate_layer_4(tmp3)



        input_to_skip_1 = self.pool_for_skip_connections(tmp1)
        input_to_skip_1 = self.skip_connection_conv_1(input_to_skip_1)
        input_to_skip_2 = self.skip_connection_conv_2(tmp2)
        input_to_skip_3 = self.skip_connection_conv_3(tmp3)
        input_to_skip_4 = self.skip_connection_conv_4(tmp4)
        input_to_skip_4 = self.upsample_layer(input_to_skip_4)

        tmp = torch.cat([input_to_skip_1, input_to_skip_2, input_to_skip_3, input_to_skip_4], dim=1)


        cnn_tmp = self.skip_connection_conv_5(tmp)
        # shape of tmp of cnn is  torch.Size([1, 128, 28, 28])
        # print("shape of tmp of cnn is ", cnn_tmp.shape)



        dummy_zero = torch.zeros([batch_size, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]).float().to('cuda')
        gt_vertices = torch.zeros([batch_size, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]).float().to('cuda')
        shifted_gt_vertices = torch.zeros([batch_size, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE]).float().to('cuda')



        # shape of concatenated cnn_tmps is  torch.Size([1, 1, 131, 28, 28])
        tmp = torch.cat([cnn_tmp.unsqueeze(1), dummy_zero, gt_vertices, shifted_gt_vertices],
                           dim=2)
        # print("shape of concatenated cnn_tmps is ", tmp.shape)
        ###########################################################################

        tmp, context_1 = self.conv_lstm_layer(tmp)
        # print("Shape of context_1 is ", context_1.size())

        # concerned with tmp of last LSTM only
        tmp = tmp[-1]

        # Shape of tmp last batch is is  torch.Size([1, 1, 8, 28, 28])
        # print("Shape of tmp last batch is is ", tmp.size())

        # flatten the tmp: [1, 1, 6272]
        tmp = tmp.contiguous().view(batch_size, 1, -1)
        # print("Shape of tmp after CONT stuff is ", tmp.size())




        ##############################################################
        sliding_2 = torch.zeros([batch_size, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE + 3]).to('cuda')
        # sliding_2 = torch.zeros([batch_size, 1, 28 * 28 + 3])
        # first end token
        sliding_2[:, 0, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE + 1] = 1


        ########################################################
        sliding_3 = torch.zeros([batch_size, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE + 3]).to('cuda')
        # sliding_3 = torch.zeros([batch_size, 1, 28 * 28 + 3])
        # second end token
        sliding_3[:, 0, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE * self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE + 2] = 1

        ##################################################
        # [1, 1, 7846]
        tmp = torch.cat([tmp, sliding_2, sliding_3], dim=2)
        # print("Shape of tmp fed to LSTM is ",  tmp.size())

        tmp, context_2 = self.lstm_layer(tmp)
        tmp = tmp.contiguous().view(batch_size, -1)
        tmp = self.linear(tmp)
        tmp = tmp.contiguous().view(batch_size, 1, -1)

        # [1, 1, 787]
        # print("Shape of final tmp from lstm+linear is ", tmp.size())

        v_pred=tmp.max(dim=2, keepdim=True)
        # print("v_pred is ", v_pred)
        gt_one=self.fetch_flattened_coordinates(gt_list[0][0], gt_list[0][1])
        tmp = (tmp == v_pred[0]).float()
        tmp[:,:,:]=0
        tmp[0][0][gt_one]=1

        # let this be the first vertex permanently
        sliding_1 = tmp

        # store this in the model_predictions as well
        # print("stored in first vertex thingy is ",tmp.argmax(2))
        model_predictions[:, 0] = (tmp.argmax(2))[:, 0]
        model_predictions[:, 0] = gt_one
        # return
        
        corrections_done=1

        for i in range(1,max_timesteps):

            # print("LOOP TIMESTEP IS ", i)

            # (last vertex of prev timestep ) becomes second last vertex of this timestep
            sliding_2 = sliding_3

            # (tmp of previous timestep becomes last vertex of this timestep) 
            sliding_3 = tmp


            all_gt_vertices_one_hot = sliding_1[:, :, :-3].view(-1, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)
            gt_vertices = sliding_2[:, :, :-3].view(-1, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)
            shifted_gt_vertices = sliding_3[:, :, :-3].view(-1, 1, 1, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)
            # print("curr sliding_1 is ", self.fetch_max(sliding_1))
            # print("curr sliding_3 is ", self.fetch_max(sliding_3))
            # print("curr sliding_2 is ", self.fetch_max(sliding_2))

            input1 = torch.cat(
                [cnn_tmp.unsqueeze(1), all_gt_vertices_one_hot, gt_vertices, shifted_gt_vertices], dim=2)

            # apply lstm
            tmp, context_1 = self.conv_lstm_layer(input1, context_1)
            # we only care about tmp of last layer
            tmp = tmp[-1]


            tmp = tmp.contiguous().view(batch_size, 1, -1)
            tmp = torch.cat([tmp, sliding_2, sliding_3], dim=2)
            tmp, context_2 = self.lstm_layer(tmp, context_2)
            tmp = tmp.contiguous().view(batch_size, -1)
            tmp = self.linear(tmp)
            tmp = tmp.contiguous().view(batch_size, 1, -1)
            tmp = (tmp == tmp.max(dim=2, keepdim=True)[0]).float()

            curr_pred_vertex=self.fetch_max(tmp)
            v_x, v_y=self.fetch_2d(curr_pred_vertex)

            if i<len(gt_list):
                gt_want=gt_list[i]
            else:
                gt_want=(0, self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE)
            # if  (i<len(gt_list)) and (corrections_done<corrections_allowed) and (self.fetch_manhattan((v_x, v_y),gt_want )>=THRESHOLD):
            #     # do ocourse correction
            #     corrections_done+=1
            #     tmp[:,:,:]=0
            #     tmp[0][0][self.fetch_flattened_coordinates(gt_want[0], gt_want[1])]=1

            if  (corrections_done<corrections_allowed) and (self.fetch_manhattan((v_x, v_y),gt_want )>=THRESHOLD):
                # do ocourse correction
                corrections_done+=1
                # print("CHANGE MADE")
                tmp[:,:,:]=0
                tmp[0][0][self.fetch_flattened_coordinates(gt_want[0], gt_want[1])]=1
            
            model_predictions[:, i] = (tmp.argmax(2))[:, 0]

            # print("#################################")

        return model_predictions
