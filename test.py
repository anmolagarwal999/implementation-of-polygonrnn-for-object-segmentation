import json
import os
from glob import glob

import numpy as np
import torch.utils.data
from torch.autograd import Variable
from PIL import Image, ImageDraw
from torch import nn

from dataset_creation_utils import *

from model import PolygonRnn

def check_test():
    # parser = argparse.ArgumentParser(description='manual to this script')
    # parser.add_argument('--gpu_id', nargs='+', type=int)
    # parser.add_argument('--num', type=int, )
    # parser.add_argument('--model', type=str)
    # parser.add_argument('--dataset', type=str)
    # parser.add_argument('--config', dest='config_file', help='Config File')
    # args = parser.parse_args()

    # config_from_args = args.__dict__
    # config_file = config_from_args.pop('config_file')
    # config = get_config('test', config_from_args, config_file)

    # devices = config['gpu_id']
    # num = config['num']
    # dataset = config['dataset']
    # model = config['model']

    # print('gpus: {}'.format(devices))
    # torch.cuda.set_device(devices[0])

    path_to_model="/home/anmolagarwal/Desktop/mounted_dump/cv_project/major_training_code/major/weights_sent_by_vedansh/train414_50000.pth"
    dataset='train'
    net = PolygonRnn(load_vgg=False)
    
    net = nn.DataParallel(net, device_ids=None)
    net.load_state_dict(torch.load(path_to_model,map_location='cpu'), strict=False)
    # net.cuda()
    print('Loading completed!')
    # num is basically number of scenes
    iou_score = test(net, dataset, num=1)
    print('iou score:{}'.format(iou_score))
    return iou_score

def fetch_main_img_path(cityscapes_json_path):
    s1="gtFine_trainvaltest"
    s2="gtFine"
    s3='leftImg8bit_trainvaltest'
    s4='leftImg8bit'
    # aachen_000000_000019_leftImg8bit.png
    # aachen_000000_000019_gtFine_polygons.json
    img_path=cityscapes_json_path.replace(s1, s3).replace(s2, s4).replace('json','png').replace("_polygons","")
    # print(img_path)
    return img_path



def fetch_revised_vertices_2(old_vertices:List[Tuple[int,int]],
                            org_size:Tuple[int,int],new_size:Tuple[int,int], 
                            remove_duplicates:bool=False, RECV_GAP_FRACTION=0)->List[Tuple[int,int]]:
    '''
    Receives vertices in coordinate frame corresponding to (org_size)
    It then first crops keeping in mind the GAP FRACTION.
    After that, it fetches revised vertices in NEW COORDINATE FRAME (ie the one corresponding to NEW_SIZE)
    '''
    old_w, old_h = org_size
    new_w, new_h = new_size
    # print("new size is ", new_size)

    if not remove_duplicates:
        # print("WARNING: duplicates are currently not being removed")
        pass

    # print(f"new w : {new_w}, new_h: {new_h}")

    # min_w, max_w, min_h, max_h = fetch_extremas(old_vertices, gap_fraction=RECV_GAP_FRACTION, img_size=org_size)

    min_h,min_w,max_h,max_w = getbboxfromkps(np.array(old_vertices), old_h, old_w, f=RECV_GAP_FRACTION)

    # print(f"mh:{min_h} mw:{min_w} max_h:{max_h} max_w:{max_w}")



    middle_w, middle_h = (max_w-min_w+1 , max_h-min_h+1)
    # print(f"Stuff in revision: {min_w}: {max_w}: {min_h}: {max_h}")
    
    revised_vertices:List[Tuple[int, int]]=[]
    # print("original list of vertcies is ", len(old_vertices))

    already_there:Set=set()
    for (curr_w, curr_h) in old_vertices:
        # print("curr is ", curr_w, curr_h)
        curr_w-=min_w
        curr_h-=min_h
        # print("sub: ", min_w, min_h)
        # print("after sub  curr is ", curr_w, curr_h)


        '''
        curr_w between (0 and middle_w-1)
        curr_w/middle_w between (0, [middle_w-1]/[middle_w])
        new curr_w between 0 and 

        '''

        curr_w*=new_w/middle_w
        curr_h*=new_h/middle_h
        # print("inter curr is ", curr_w, curr_h)

        curr_w=round(curr_w)
        curr_h=round(curr_h)
        curr_w=min(curr_w, new_w-1)
        curr_h=min(curr_h, new_h-1)
        # print("new curr is ", curr_w, curr_h)
        # print("######")

        
        
        if remove_duplicates:
            if (curr_w, curr_h) not in already_there:
                already_there.add((curr_w, curr_h))
                revised_vertices.append((curr_w, curr_h))
        else:
            revised_vertices.append((curr_w, curr_h))
    # print("revised len is ", len(revised_vertices))
    # print("revised are ", revised_vertices)
    return revised_vertices


def do_stuff(net, json_path, json_id, DEST_PATH,PATH_TO_CITYSCAPES_MAIN, corrections_allowed, threshold):

    dtype = torch.cuda.FloatTensor
    dtype_t = torch.cuda.LongTensor


    # dtype = torch.FloatTensor
    # dtype_t = torch.LongTensor
    ############################################################
    # load the df which contains label
    # print("json path is ", json_path,"\n")
    with open(json_path,'r') as fd:
        df=json.load(fd)

    # fetch obj_id within scene
    obj_id=df["object_idx_within_scene"]

    # fetch path to original JSON in the CityScapes dataset
    path_to_json_for_original_image=df["path_to_json_for_original_image"]
    # handle environment (SLURM or local)
    path_to_json_for_original_image=path_to_json_for_original_image.replace("/home/anmolagarwal/Desktop/mounted_dump/cv_project/main_cityscapes_dataset",PATH_TO_CITYSCAPES_MAIN)

    # fetch path to original image in the CityScapes dataset
    path_to_original_image=fetch_main_img_path(path_to_json_for_original_image)
    # print("path to original image is ", path_to_original_image)

    # import MAIN GT JSON
    with open(path_to_json_for_original_image,'r') as fd:
        main_df=json.load(fd)
    #####################################
    original_img = Image.open(path_to_original_image).convert('RGB')

    ################################
    org_h = main_df['imgHeight']
    org_w = main_df['imgWidth']
    FRAC=0.1
    # find the ground truth vertices in original space
    gt_org_vertices=main_df['objects'][obj_id]['polygon']

    # fetch the vertices in 28*28 space
    vertices_28=fetch_revised_vertices_2(gt_org_vertices, (org_w, org_h), (28,28), RECV_GAP_FRACTION=FRAC, remove_duplicates=True)
    # print("vertices 28 are ", vertices_28)

    ##########################################################
    # get the coordinates where the CROP is to be performed
    min_row, min_col, max_row, max_col = getbboxfromkps(
                    gt_org_vertices, org_h, org_w, f=FRAC)
    # print(f"OUT mh:{min_row} mw:{min_col} max_h:{max_row} max_w:{max_col}")
    
    # crop with and height
    object_h = max_row - min_row+1
    object_w = max_col - min_col+1

    # find scales to multiply with to convert to 224 * 224 space
    scale_h = 224.0 / object_h
    scale_w = 224.0 / object_w
    ##############################################
    
    # ground truth 
    img_gt = Image.open(path_to_original_image).convert('RGB')
    # img_gt_28 = Image.open(path_to_original_image).convert('RGB')
    img = Image.open(path_to_original_image).convert('RGB')
    I = np.array(img)
    # print("Size of I is ", I.size)

    ##################################


    # cropped image
    I_obj = I[min_row:max_row, min_col:max_col, :]
    # print("Cropped Size of I_obj is ", I_obj.size)

    I_obj_img = Image.fromarray(I_obj)

    # resize cropped image to 224 * 224
    I_obj_img = I_obj_img.resize((224, 224), Image.BILINEAR)

    # resize cropped image to 28 * 28
    # I_obj_img_28 = I_obj_img.resize((28, 28), Image.BILINEAR)

    # I_obj_img.save("in_second.png",'PNG')

    # get image 224 * 224 embedding to be fed into CNN
    I_obj_new = np.array(I_obj_img)
    # print("Cropped Size of I_obj_new is ", I_obj_new.size)

    # convert image to tensor
    cnn_input = torch.from_numpy(np.rollaxis(I_obj_new,2,0))
    # print("shape of tensor is ", cnn_input.size())

    # add a dummy batch size
    cnn_input = cnn_input.unsqueeze(0).type(dtype)

    cnn_input = Variable(cnn_input)
    # print("shape of cnn_input is ", cnn_input.size())

    # testing pipeline
    re = net.module.test(cnn_input, 60, vertices_28, corrections_allowed, threshold)
                # return net

    # print("shape of re is ", re.size())


    labels_p = re.cpu().numpy()[0]
    # print("type of labels_p is ", type(labels_p))
    # print("labels are ", labels_p)

    ##########################################################
    vertices1 = []
    vertices2 = []
    # vertices3 = []

    # choose a random color
    # color = [np.random.randint(0, 255) for _ in range(3)]

    # add one more dimension to the color
    # color += [100]
    # color = tuple(color)
    # print("color is ", color)

    #####################################
    # vertices 1 stored the predicted labels
    for label in labels_p:
        if (label == 784):
            break
        vertex = (
            ((label % 28) * 8.0 + 4) / scale_w + min_col, (
                (int(label / 28)) * 8.0 + 4) / scale_h + min_row)
        vertices1.append(vertex)

    try:
        # drw = ImageDraw.Draw(img, 'RGBA')
        # drw.polygon(vertices1, color)
        pass
    except TypeError:
        pass
    #################################
    #####################################
    # for gt 28
    # vertices_28 stores the GT labels
    # for (y,x) in vertices_28:
    #     vertex = (
    #         (y * 8.0 +4) / scale_w + min_col, (
    #             (x) * 8.0 +4) / scale_h + min_row)
    #     vertices3.append(vertex)

    # try:
    #     drw = ImageDraw.Draw(img_gt_28, 'RGBA')
    #     drw.polygon(vertices3, color)
    # except TypeError:
    #     pass
    #################################

    #####################################
    for points in gt_org_vertices:
        vertex = (points[0], points[1])
        vertices2.append(vertex)
    ################################

    # drw_gt = ImageDraw.Draw(img_gt, 'RGBA')
    # drw_gt.polygon(vertices2, color)
    iou_val, nu_this, de_this = iou(vertices1, vertices2, org_h, org_w)

    ##############################
    # drw_gt = ImageDraw.Draw(I_obj_img_28, 'RGBA')
    # drw_gt.polygon(vertices_28, color)
    # I_obj_img_28=draw_polygon_on_image(I_obj_img_28, vertices_28, draw_vertices=True)
    ############################

    dataset='train'
    ind=json_id
    ###########################################################
    # dir_name = 'save_img/tmp_2' + dataset + '/'
    # img.save(dir_name + str(ind) + '_pred.png', 'PNG')
    # img_gt.save(dir_name + str(ind) + '_gt.png', 'PNG')
    # img_gt_28.save(dir_name + str(ind) + '_gt_28.png', 'PNG')

    # I_obj_img_28.resize((228,228)).save(dir_name + str(ind) + '_28.png', 'PNG')

    json_save_path=os.path.join(DEST_PATH, f'{json_id}.json')
    new_df=df.copy()
    # print("labels type is ", type(labels_p))
    new_df['labels_p']=labels_p.tolist()
    new_df['pred_org']=vertices1
    # print("types are ", type(iou_val), type(nu_this), type(de_this))
    new_df['iou']=float(iou_val)
    new_df['num']=float(nu_this)
    new_df['denom']=float(de_this)


    with open(json_save_path, 'w') as fd:
        json.dump(new_df, fd)

    return 





