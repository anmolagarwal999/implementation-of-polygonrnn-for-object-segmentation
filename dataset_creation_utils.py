import glob
import json
import os
import random
from typing import Any, Dict, List, Tuple, Set
import numpy as np
import torch
from PIL import Image, ImageDraw
from torch.utils.data.dataset import Dataset
import torchvision.transforms as transforms


# function which takes the breed of the dataset ie train, test OR val
# it also takes the essential classes 
# since it is NOT possible to save all those classes in memory, it should be possible to store {path, index, breed}

def fetch_required_dataset(breed:str, reqd_classes:List[str], DIR_WITH_IMAGES, DIR_WITH_POLYGON_COORDINATES)->List[Tuple[str, int, str]]:
    '''
    Fetches all (objects) alongwith paths and ground truth vertices from a particular BREED (train, test, val) only if the object type is in the list of reqd classes
    '''
    # stores (path to json file, object id within the image, object category)
    arr:List[Tuple[str, int, str]]=[]

    # find all ground truth files present in the folder in the specified `breed` 
    json_paths: List[str] = glob.glob(
            os.path.join(DIR_WITH_POLYGON_COORDINATES, 'gtFine', breed, '*', '*.json')
        )
    json_paths=sorted(json_paths)
    print("Breed is ", breed)
    print("Number of JSON paths retrieved is ", len(json_paths))

    for json_path in json_paths:
        # file_id = 'munich_000187_000019_gtFine_'
        # print("basename is ", os.path.basename(json_path))
        file_id: str = os.path.basename(json_path)[:-21]
        cityname: str = file_id.split('_')[0]

        # print("file id and cityname are ", file_id, cityname)

        # path to the actual image
        img_path = os.path.join(
            DIR_WITH_IMAGES, 'leftImg8bit', breed, cityname,
            f'{file_id}_leftImg8bit.png'
        )

        if os.path.exists(img_path) and os.path.exists(json_path):
            with open(json_path, 'r') as fd:
                df=json.load(fd)
            # fetch all objects
            tmp_arr=[(json_path, idx, x['label']) for (idx, x) in enumerate(df['objects'])]

            # fetch only objects WHICH have a desired class
            tmp_arr=list(filter(lambda x:x[2] in reqd_classes, tmp_arr))
            arr.extend(tmp_arr)        
        
        else:
            raise FileNotFoundError(f"Image not found at {img_path}")

    return arr

############################################################################################

# Implementation of a proper dataset class with __len__, __getitem__ etc.
# We make the CLASS inherit the properties of torch.utils.data.Dataset so that we can later leverage nice functionalities such as multiprocessing.
# a nice tutorial on DataLoaders by STANFORD: https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel
class dataset_class(Dataset):
    def __init__(
        self, 
        phase:str,
        max_seq_len :int = 60 , 
        transform=None,
        DIR_WITH_IMAGES:str='../leftImg8bit_trainvaltest/',
        DIR_WITH_POLYGON_COORDINATES:str='../gtFine_trainvaltest/'
        ):

        #  TODO: find meaning
        '''
        Page 5 in the paper: Based on this statistics, we choose a hard limit of 70 time steps for our RNN, taking also GPU memory requirements into account.
        '''

        # max number of vertices allowed (inclusive of the end token)
        self.max_seq_len = max_seq_len
        self.transform = transform
        assert(phase in ['train','val','test'])
        self.phase=phase

        

        self.DIR_WITH_IMAGES=os.path.join(DIR_WITH_IMAGES,phase)
        self.DIR_WITH_POLYGON_COORDINATES=os.path.join(DIR_WITH_POLYGON_COORDINATES,phase)
        # self.object_paths=object_paths
        self.object_paths=sorted(glob.glob(os.path.join(self.DIR_WITH_POLYGON_COORDINATES,"*")), key=lambda x:(len(x),x))
        print("number of Objects in the dataset: ", len(self.object_paths))

    def __len__(self) -> int:
        # Number of images.
        return len(self.object_paths)
    
    def __getitem__(self, seeked_index):
        # Let us be concerned about object "O" in image "I"

        # (path to JSON files where ground truth for "I" is stored, 
        # IDX of "O" in image "I"
        #    CATEGORY OF THE OBJECT   )
        json_path = self.object_paths[seeked_index] 

        ###########################################
        # import poygon coordinates
        with open(json_path, 'r') as fd:
            df=json.load(fd)
            # vertices:List[Tuple[int,int]]=[(x,y) for (x,y) in df['objects'][idx]['polygon']]
        # print("df is ", df)
        # print("number of vertices are: ", len(vertices))
        # print("vertcies are ", vertices)        
        ############################################
        img_224_path=df['path_to_saved_224_image']
        object_idx_within_scene=df['object_idx_within_scene']
        object_label=df['object_label']
        vertices:List[Tuple[int,int]]=[(x,y) for (x,y) in df['vertices_in_28_space']]

        # print(f"Path is {json_path} : Object ID is {object_idx_within_scene} : Label: {object_label}")

        # return df
        # return 1
        
        # annotate object 
        img_224: Image.Image = Image.open(img_224_path).convert('RGB')
        # print("Dimensions of Original image is ", img_224.size)

        # Define a transform to convert PIL 
        # image to a Torch tensor
        transform = transforms.Compose([
            transforms.PILToTensor()
        ])
        
        # transform = transforms.PILToTensor()
        # Convert the PIL image to Torch tensor
        img_tensor = transform(img_224)

        # first should simply be the image
        # return img_224, img_tensor

        ##############################################################
        # one extra needed for end token
        ground_truth_one_hot = np.zeros([self.max_seq_len, 28 * 28 + 1])
        # print("Number of vertices (without END): ", len(vertices))
        FLATTENED_INDEX_OF_END_TOKEN=28*28
        PAIRED_INDEX_OF_END_TOKEN=(0,28)
        vertices.append(list(PAIRED_INDEX_OF_END_TOKEN))
        
        # print("Number of vertices (INCLUDING END): ", len(vertices))

        wanted_indices=[-1]*self.max_seq_len

        for timestep in range(min(   len(vertices), self.max_seq_len)) :
            (x,y)=vertices[timestep]
            # print("(x,y) is ", (x,y))
            flattened_idx=28*y+x
            # print("flattened is ", flattened_idx)
            ground_truth_one_hot[timestep, flattened_idx]=1
            wanted_indices[timestep]=flattened_idx

        ground_truth_one_hot=torch.tensor(ground_truth_one_hot)

        # v1, v2, v3, ...., v_n

        # send one hot encoding of the first vertex

        # create one hot encoding for each ground truth except the left over ones

        # Create one hot encoding for all vectors and number of vertices including the end token

        # send image, one hot encoding AND number of vertices

        first_vertex_one_hot=ground_truth_one_hot[0].clone()
    
        return   img_tensor, first_vertex_one_hot, ground_truth_one_hot, torch.tensor(wanted_indices), len(vertices)

        # return [seeked_index,7],[seeked_index*10, seeked_index*100], seeked_index

    
        # return img, entire_annotated_img, just_obj_img, resized_cropped_image, annotated_cropped_image, downsampled_image, annotated_downsampled_image
        # return [seeked_index]*10, [seeked_index//10]*5
        # return np.asarray(resized_cropped_image), downsampled_vertices

    
############################################

def draw_polygon_on_image(img:Image.Image, polygon_coordinates:List[Tuple[int, int]], draw_vertices:bool=False)->Image.Image():
    '''
    Input: Image and list of pixel coordinates of polygon vertices in sequence
    Output: Image with polygon drawn over it
    '''
    # print("number of vertices to be annotated are: ", len(polygon_coordinates))
    draw = ImageDraw.Draw(img)
    line_thickness=1
    draw_coords=polygon_coordinates[:]
    draw_coords.append(draw_coords[0])
    draw.line(draw_coords, fill="red", width=line_thickness)
    if draw_vertices:
        COL_SHADE=[0,0,0]
        for (curr_w, curr_h) in polygon_coordinates:
            # draw.point((curr_w, curr_h), 'green')
            GAP=1
            COL_SHADE[1]+=256/len(polygon_coordinates)
            COL_SHADE[1]=int(COL_SHADE[1])
            draw.ellipse((curr_w-GAP, curr_h-GAP,curr_w+GAP,  curr_h+GAP),fill=tuple(COL_SHADE),  outline =tuple(COL_SHADE))
            # print("done")
            # break

    return img

def fetch_revised_vertices(old_vertices:List[Tuple[int,int]],
                            org_size:Tuple[int,int],new_size:Tuple[int,int], 
                            remove_duplicates:bool=False, RECV_GAP_FRACTION=0)->List[Tuple[int,int]]:
    '''
    Receives vertices in coordinate frame corresponding to (org_size)
    It then first crops keeping in mind the GAP FRACTION.
    After that, it fetches revised vertices in NEW COORDINATE FRAME (ie the one corresponding to NEW_SIZE)
    '''
    old_w, old_h = org_size
    new_w, new_h = new_size

    if not remove_duplicates:
        # print("WARNING: duplicates are currently not being removed")
        pass

    # print(f"new w : {new_w}, new_h: {new_h}")

    min_w, max_w, min_h, max_h = fetch_extremas(old_vertices, gap_fraction=RECV_GAP_FRACTION, img_size=org_size)
    middle_w, middle_h = (max_w-min_w+1 , max_h-min_h+1)
    # print(f"Stuff in revision: {min_w}: {max_w}: {min_h}: {max_h}")
    
    revised_vertices:List[Tuple[int, int]]=[]
    # print("original list of vertcies is ", len(old_vertices))

    already_there:Set=set()
    for (curr_w, curr_h) in old_vertices:
        # print("curr is ", curr_w, curr_h)
        curr_w-=min_w
        curr_h-=min_h

        '''
        curr_w between (0 and middle_w-1)
        curr_w/middle_w between (0, [middle_w-1]/[middle_w])
        new curr_w between 0 and 

        '''

        curr_w*=new_w/middle_w
        curr_h*=new_h/middle_h
        curr_w=round(curr_w)
        curr_h=round(curr_h)
        curr_w=min(curr_w, new_w-1)
        curr_h=min(curr_h, new_h-1)
        if remove_duplicates:
            if (curr_w, curr_h) not in already_there:
                already_there.add((curr_w, curr_h))
                revised_vertices.append((curr_w, curr_h))
        else:
            revised_vertices.append((curr_w, curr_h))
    # print("revised len is ", len(revised_vertices))
    # print("revised are ", revised_vertices)
    return revised_vertices




def fetch_extremas(polygon_coordinates:List[Tuple[int,int]], gap_fraction=0, img_size=None)->Any:

    '''
    This function is supposed to find the pixel coordinates of the cropped image.
    In an exact cropping case, atleast one vertex will lie on all 4 edges of the rectange.
    Since a human cannot draw such accurate bounding boxes, there is a provision to make a larger bounding box where how large the box should be made depends on CROP FRACTION
    '''


    min_w=min([x[0] for x in polygon_coordinates])
    max_w=max([x[0] for x in polygon_coordinates])

    min_h=min([x[1] for x in polygon_coordinates])
    max_h=max([x[1] for x in polygon_coordinates])

    dw=max_w-min_w
    dh=max_h-min_h

    min_w-=gap_fraction*dw
    max_w+=gap_fraction*dw

    min_h-=gap_fraction*dh
    max_h+=gap_fraction*dh

    min_w=max(int(min_w), 0)
    min_h=max(int(min_h), 0)

    max_w=min(int(max_w),img_size[0]-1)
    max_h=min(int(max_h),img_size[1]-1)

    return (min_w, max_w, min_h, max_h)


def preprocess_object_and_save(target_index, json_path, idx, obj_label, path_to_dir_with_images, GAP_FRACTION,
                                                                        curated_folder_path):
    # Let us be concerned about object "O" in image "I"

    # (path to JSON files where ground truth for "I" is stored, 
    # IDX of "O" in image "I"
    #    CATEGORY OF THE OBJECT   )

    # making sure that object is a wanted one

    # print(f"Path is {json_path} : Object ID is {idx} : Label: {obj_label}")

    phase=json_path.split("/")[-3]
    # print("phase is ", phase)
    ######################################################################
    # now we need to import the image to send to the CNN for embedding generation
    file_id: str = os.path.basename(json_path)[:-21]
    cityname: str = file_id.split('_')[0]

    img_path=os.path.join(
            path_to_dir_with_images, 'leftImg8bit', phase, cityname,
            f'{file_id}_leftImg8bit.png'
        )
    ###############################################################

    ###########################################
    # import poygon coordinates
    with open(json_path, 'r') as fd:
        df=json.load(fd)
        vertices:List[Tuple[int,int]]=[(x,y) for (x,y) in df['objects'][idx]['polygon']]
    # print("number of vertices are: ", len(vertices))
    # print("vertcies are ", vertices)        
    ############################################
    # annotate object 
    img: Image.Image = Image.open(img_path).convert('RGB')
    # print("Dimensions of Original image is ", img.size)

    ##########################################
    # ANNOTATE ON THE entire image
    # entire_annotated_img:Image.Image=img.copy()
    # entire_annotated_img = draw_polygon_on_image(entire_annotated_img, vertices, draw_vertices=True)

    # GET THE CROPPED image
    min_w, max_w, min_h, max_h = fetch_extremas(vertices, gap_fraction=GAP_FRACTION , img_size=img.size)
    crop_shape: Tuple[int, ...] = (min_w, min_h, max_w, max_h)
    just_obj_img:Image.Image()=img.copy()
    just_obj_img = just_obj_img.crop(crop_shape)

    # RESIZE THE CROPPED IMAGE TO (224, 224)
    RESIZED_SHAPE=(224, 224)
    resized_cropped_image=just_obj_img.resize(RESIZED_SHAPE)
    original_shape=img.size

    # fetch revised pixel coordinates of the vertices
    revised_vertices=fetch_revised_vertices(vertices, original_shape, RESIZED_SHAPE, RECV_GAP_FRACTION=GAP_FRACTION)
    # print(f"Num vertices after resizing to {RESIZED_SHAPE} : {len(revised_vertices)}")

    # annotated_cropped_image = resized_cropped_image.copy()
    # annotated_cropped_image = draw_polygon_on_image(annotated_cropped_image, revised_vertices, draw_vertices=True)

    ##################################################
    downsampled_image=just_obj_img.copy()
    DOWNSAMPLED_SIZE=(28, 28)
    downsampled_image=downsampled_image.resize(DOWNSAMPLED_SIZE)

    downsampled_vertices=fetch_revised_vertices(vertices, original_shape, DOWNSAMPLED_SIZE, remove_duplicates=True, RECV_GAP_FRACTION=GAP_FRACTION)
    print(f"Num vertices after downsampling to {DOWNSAMPLED_SIZE} : {len(downsampled_vertices)}")


    # annotated_downsampled_image = downsampled_image.copy()
    # annotated_downsampled_image = draw_polygon_on_image(annotated_downsampled_image, downsampled_vertices, draw_vertices=True)

    # return [target_index,7],[target_index*10, target_index*100], target_index
    save_path_for_json=os.path.join(curated_folder_path,"jsons",phase,f"{target_index}.json")
    save_path_for_image=os.path.join(curated_folder_path,"images",phase,f"{target_index}.png")
    # save_path_for_image_dummy=os.path.join(curated_folder_path,"images",phase,f"{target_index}_dummy.jpg")


    resized_cropped_image.save(save_path_for_image)
    # annotated_downsampled_image.resize(RESIZED_SHAPE).save(save_path_for_image_dummy)

    # Path to json for original image
    # Object idx within the scene
    # Object label
    # List of vertex coordinates in the 28 * 28 space
    # The image in 224 * 224 space
    json_obj={
        "path_to_json_for_original_image":json_path,
        "object_idx_within_scene":idx, 
        "object_label":obj_label,
        'gap_fraction_used':GAP_FRACTION,
        "path_to_saved_224_image":save_path_for_image,
        'vertices_in_28_space':downsampled_vertices,
        'vertices_in_224_space':revised_vertices,
        'original_vertices':vertices
    }

    with open(save_path_for_json,'w') as fd:
        json.dump(json_obj, fd)

    return


    

    # return img, entire_annotated_img, just_obj_img, resized_cropped_image, annotated_cropped_image, downsampled_image, annotated_downsampled_image