
import os
import sys
import json

import torch
import torch.nn.init
import torch.utils.model_zoo as model_zoo
import wget
from torch import nn
from torch import optim

import torch.utils.data
from torch import nn
from torch.autograd import Variable
from utils.convlstm import ConvLSTM


from model import PolygonRnn
import wget
import torch.nn.functional as F


from pathlib import Path
from tensorboardX import SummaryWriter
#os.environ["CUDA_VISIBLE_device_id"] = "-1"


from dataset_creation_utils import *


def load_data(batch_size):
    
    with open("config_stuff/dataset_creation.json",'r') as fd:
        df=json.load(fd)
    

    print("inside load data function")
    # https://stackoverflow.com/a/64629989
    trans = transforms.ToTensor()

    dataset_obj=dataset_class(
        phase='train',
        max_seq_len = 60 , 
        transform=None,
        DIR_WITH_IMAGES=os.path.join(df['curated_folder'], 'images'),
        DIR_WITH_POLYGON_COORDINATES=os.path.join(df['curated_folder'], 'jsons')
        )

    # drop_last: drops the last incomplete batch
    Dataloader = torch.utils.data.DataLoader(dataset_obj, batch_size=batch_size,
                                             shuffle=False, drop_last=True)
    return Dataloader


def poly_vertex_loss_mle(targets, mask, logits):
    """
    Classification loss for polygon vertex prediction

    targets: [batch_size, time_steps, grid_size**2+1]
    Each element is y*grid_size + x, or grid_size**2 for EOS
    mask: [batch_size, time_steps]
    Mask stipulates whether this time step is used for training
    logits: [batch_size, time_steps, grid_size**2 + 1]
    """
    batch_size = mask.size(0)
    
    # Remove the zeroth time step
    logits = logits[:, :, :].contiguous().view(-1, logits.size(-1)) # (batch*(time_steps-1), grid_size**2 + 1)
    targets = targets[:, :, :].contiguous().view(-1, logits.size(-1)) # (batch*(time_steps-1))
    mask = mask[:, :].contiguous().view(-1) # (batch*(time_steps-1))

    # Cross entropy between targets and softmax outputs
    loss = torch.sum(-targets * F.log_softmax(logits, dim=1), dim=1)
    loss = loss * mask.type_as(loss)
    loss = loss.view(batch_size, -1)
    # Sum across time
    loss = torch.sum(loss, dim=1)
    # Mean across batches
    return torch.mean(loss)




def train():

    alpha = 0.001
    device_id = [0]

    log_dir = "./logs_dir"


    batch_size = 128

    print('Using gpus: {}'.format(device_id))
    # Sets the current device.
    #torch.cuda.set_device(device_id[0])

    NUM_TIMESTEPS=70
    data_obj = load_data(batch_size)
    dataset_len = len(data_obj)
    experiment_name = "./cp/train_1_"
    print("len of dataloader is ",dataset_len)
    #return

    model = PolygonRnn()
    model = nn.DataParallel(model, device_ids=device_id)

    # Put none if we need to train
    pretrained = '/scratch/v/cv/anmol/a/train_1__16.pth'
    if pretrained:
        model.load_state_dict(torch.load(pretrained))
    #model.cuda()
    print('Loading completed!', flush=True)

    loss_function = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=alpha)
    scheduler = optim.lr_scheduler.MultiStepLR(optimizer,
                                               milestones=[340, 8000],
                                               gamma=0.2)
    tensorboard_writer = SummaryWriter(log_dir)

    total_epochs = int(6e5 // dataset_len)

    for curr_epoch in range(total_epochs):
        print("epoch is ",curr_epoch)
        for step, data in enumerate(data_obj):
            # print("Step number is ", step)
    
            inp_img = Variable(data[0].type(torch.cuda.FloatTensor))
            fv_onehot = Variable(data[1].type(torch.cuda.FloatTensor))
            gt_one_hot = Variable(data[2].type(torch.cuda.FloatTensor))
            indices_inp = Variable(data[3].type(torch.cuda.FloatTensor))
            idx_array = Variable(data[4].type(torch.cuda.FloatTensor))
            optimizer.zero_grad()

            res = model(inp_img, fv_onehot, gt_one_hot)
            mask=torch.zeros((batch_size, NUM_TIMESTEPS))
            for idx, curr_elem in enumerate(idx_array):
                #print(idx)
                #print(curr_elem.item())
                #print(type(curr_elem.item()))
                mask[idx,:int(curr_elem.item())]=1
            
            
            ###############################################

            result = res.contiguous().view(-1, 785)
            actual_gt_vertex_flattened_index = indices_inp.contiguous().view(-1)
            
     
            # for training
            # loss = loss_function(result, actual_gt_vertex_flattened_index)

            # for human intervention based testing
            loss=poly_vertex_loss_mle(gt_one_hot,mask,res)
         
            loss.backward()

            predicted_vertex_coordinates = torch.argmax(result, 1)

            correct = (actual_gt_vertex_flattened_index == predicted_vertex_coordinates).type(torch.cuda.FloatTensor).sum().item()
            accy = (correct + 0.0) / actual_gt_vertex_flattened_index.shape[0]
            tb_indx = curr_epoch * dataset_len + step

            optimizer.step()
            scheduler.step()

            tensorboard_writer.add_scalar('train/acc', accy, tb_indx)
            tensorboard_writer.add_scalar('train/loss_mle', loss, tb_indx)

            if step % 90 == 1:
                torch.save(model.state_dict(),   experiment_name + f"{num}.pth")

data=train()