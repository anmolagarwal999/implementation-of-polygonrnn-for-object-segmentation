#!/usr/bin/env python
# coding: utf-8

# ### AIM IS TO FIRST FINALIZE THE TESTING PIPELINE and run it for almost all images quickly

# In[1]:


# get_ipython().run_line_magic('load_ext', 'autoreload')
# get_ipython().run_line_magic('autoreload', '2')


# In[2]:


import argparse
import json
import os
import sys
from glob import glob

import numpy as np
import torch.utils.data
from PIL import Image, ImageDraw
from torch import nn
from torch.autograd import Variable

from model import PolygonRnn
from utils.utils import iou, getbboxfromkps
import glob


# In[3]:


from test import *


# In[4]:



path_to_model="./weights_sent_by_vedansh/train414_50000.pth"
path_to_model=sys.argv[1]

dataset='train'
net = PolygonRnn(load_vgg=False)

net = nn.DataParallel(net, device_ids=None)
net.load_state_dict(torch.load(path_to_model,map_location='cpu'), strict=False)
net.cuda()
print('Loading completed!')
# num is basically number of scenes


# In[5]:


#re1 ,xx1=test(net, dataset, num=1)
#test(net, dataset, num=1)


# In[6]:


PATH_TO_TAILORED_DATASET="/home/anmolagarwal/Desktop/mounted_dump/cv_project/final_stint/tailored_dataset"
PATH_TO_TAILORED_DATASET=sys.argv[2]

REGEX_PATH=os.path.join(PATH_TO_TAILORED_DATASET, 'jsons','train','*.json')
#REGEX_PATH=os.path.join(PATH_TO_TAILORED_DATASET, 'jsons')

all_jsons=glob.glob(REGEX_PATH)
print("REGEX PATH IS ", REGEX_PATH)
all_jsons=sorted(all_jsons, key=lambda x:(len(x),x))
PATH_TO_CITYSCAPES_MAIN="/home/anmolagarwal/Desktop/mounted_dump/cv_project/main_cityscapes_dataset"
PATH_TO_CITYSCAPES_MAIN=sys.argv[3]


# In[7]:


len(all_jsons)


# In[8]:


print("args is ", sys.argv)


# In[9]:


DEST_PATH='./checker_path/train'
DEST_PATH=sys.argv[4]


# In[10]:


CORRECTIONS_ALLOWED=int(sys.argv[6])
THRESHOLD=int(sys.argv[5])


# In[11]:


for use_json in all_jsons:
    json_id=int(use_json.split("/")[-1].split(".")[0])
    print("json id is ", json_id, flush=True)
    print("path is ", use_json, flush=True)
    do_stuff(net, use_json, json_id,DEST_PATH,PATH_TO_CITYSCAPES_MAIN
                                                                      , CORRECTIONS_ALLOWED, THRESHOLD)
    #break
