import wget

import torch
import torch.nn.init
import torch.utils.model_zoo as model_zoo
from torch import nn

from utils.convlstm import ConvLSTM


class FirstV_Model(nn.Module):

    def __init__(self):
        super(FirstV_Model, self).__init__()


        self.CHOSEN_KERNEL_SIZE=3
        self.PADDING_FOR_ALL_CONVOLUTIONS=1
        self.STRIDE_FOR_ALL_CONVOLUTIONS=1
        self.DIMENSION_OF_PREPROCESSED_IMAGE=128
        self.DIMENSION_OF_CNN_DOWNSAMPLED_IMAGE=28

        # architecture: https://miro.medium.com/max/1400/1*NNifzsJ7tD2kAfBXt3AzEg.png
        # conv2d: (in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True, padding_mode='zeros', device=None, dtype=None)
        # Expected input: 224 * 224 * 3
        # Expected output: 56 * 56 * 128
        self.intermediate_layer_1 = nn.Sequential(
            nn.Conv2d(3, 64, 3, 1, 1),
            nn.BatchNorm2d(64),
            nn.ReLU(), #         224+2-3+1=224
            nn.Conv2d(64, 64, 3, 1, 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),      #   224+2-3+1=224
            nn.MaxPool2d(2, 2), # 113
            nn.Conv2d(64, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1), 
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE),
            nn.ReLU(), # 112+2-3+1=112
            nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE),
            nn.ReLU(),
            nn.MaxPool2d(2, 2) # 112/2=56
        )
        self.skip_connection_conv_1 = nn.Sequential(
            nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
            nn.ReLU(),
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
        )
        #######################################################
        # Expected input: 56 * 56 * 128
        # Expected output: 28 * 28 * 256
        self.intermediate_layer_2 = nn.Sequential(
            nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, 256, 3, 1, 1), # 56 -3 + 2 + 1 =56
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, 3, 1, 1), # 56 -3 + 2 + 1 =56
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, 3, 1, 1),# 56 -3 + 2 + 1 =56
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.MaxPool2d(2, 2) # 56/2=28
        )
        self.skip_connection_conv_2 = nn.Sequential(
            nn.Conv2d(256, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
            nn.ReLU(),
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
        )
        #######################################################
        # Expected input: 28 * 28 * 256
        # Expected output: 28 * 28 * 512
        self.intermediate_layer_3 = nn.Sequential(
            nn.Conv2d(256, 512, 3, 1, 1), # 28-3+2+1= 28
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, 3, 1, 1), # 28-3+2+1= 28
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, 3, 1, 1), # 28-3+2+1= 28
            nn.BatchNorm2d(512),
            nn.ReLU()
        )
        self.skip_connection_conv_3 = nn.Sequential(
            nn.Conv2d(512, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
            nn.ReLU(),
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
        )
        # Maxpool not included as we want the input to the skip connections to be 28 * 28. Applying max pool here would need skip connections layer to get an input of 14 * 14 from this branch.
        # paper does not mention anything about removing this max pool layer.
        # So, I shift the max pool layer to the next CONV chunk.
        #######################################################
        # Expected input: 28 * 28 * 512
        # Expected output: 14 * 14 * 512
        self.intermediate_layer_4 = nn.Sequential(
            nn.MaxPool2d(2, 2), # 28/2=14
            nn.Conv2d(512, 512, 3, 1, 1),
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, 3, 1, 1), # 28 -3+2+1=28
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.Conv2d(512, 512, 3, 1, 1), # 28 -3+2+1=28
            nn.BatchNorm2d(512),
            nn.ReLU()
        )
        self.skip_connection_conv_4 = nn.Sequential(
            nn.Conv2d(512, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
            nn.ReLU(),
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
        )
        self.pool_for_skip_connections = nn.MaxPool2d(2, 2).to('cuda')
        # NOTE: paper mentions: . We first remove the fully connected layers as well as the last max-pooling layer, pool5.
        #######################################################

        self.conv_from_skip_connection_to_rnn = nn.Sequential(
            nn.Conv2d(512, self.DIMENSION_OF_PREPROCESSED_IMAGE, 3, 1, 1),
            nn.ReLU(),
            nn.BatchNorm2d(self.DIMENSION_OF_PREPROCESSED_IMAGE)
        )
        ################################
        # self.pool_for_skip_connections = nn.MaxPool2d(2, 2)
        ##################################

        ###############################################################
        # Authors mention mode = bilinear
        # Since we use features from multiple skiplayers which have different spatial dimensions, we employ bilinear upsampling or max-pooling in order to get outputs that all have the same spatial resolution. 

        self.upsample_layer = nn.Upsample(scale_factor=2, mode='bilinear').to('cuda')
        ######################################################

        
        self.v_conv = nn.Sequential(
            nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, 1, 3, padding='same'),
            nn.ReLU()
        )
        self.e_conv = nn.Sequential(
            nn.Conv2d(self.DIMENSION_OF_PREPROCESSED_IMAGE, 1, 3, padding='same'),
            nn.ReLU()
        )

        self.vgg.load_state_dict('vgg16.pth')




    def shift_by_x(self,in_tensor, x):
        num_elems_in_batch=in_tensor.shape[0]
        tot_elems=in_tensor.shape[2]
        padding_f = torch.zeros([num_elems_in_batch, x, tot_elems], device='cuda')
        # tmp=in_tensor[:, :-x, :].view(num_elems_in_batch,-1, 1, 28, 28)
        tmp=in_tensor[:, :-x, :]
        input_f = torch.cat([padding_f, tmp], dim=1)
        return input_f

    def forward(self, resized_img, g_truth,shifted_g_truth):


        '''
        details:
        first = one hot encoding (flattened) of first vertex
        g_truth: contains one hot encoding of all vertices for all timesteps

        '''

        g_truth=self.shift_by_x(g_truth, 2)
        shifted_g_truth=self.shift_by_x(g_truth, 1)

        # print("Shape of resized_img is ", resized_img.size())
        # print("Shape of g_truth is ", g_truth.size())
        # print("Shape of shifted_g_truth is ", shifted_g_truth.size())

        num_elems_in_batch = g_truth.shape[0]
        # print("num_elems_in_batch is ", num_elems_in_batch)

        ##########################################################
        # FIRST CONV BLOCK
        block_1_output = self.intermediate_layer_1(resized_img)
        # SECOND CONV BLOCK
        block_2_output = self.intermediate_layer_2(block_1_output)
        # THIRD CONV BLOCK
        block_3_output = self.intermediate_layer_3(block_2_output)
        # 4th CONV BLOCK
        block_4_output = self.intermediate_layer_4(block_3_output)
        ##############################################################

        # operations for skip connection 1
        input_to_skip_1 = self.pool_for_skip_connections(block_1_output)
        input_to_skip_1 = self.skip_connection_conv_1(input_to_skip_1)
        # print("Shape of input_to_skip_1 is ", input_to_skip_1.size())


        # operations for skip connection 2
        input_to_skip_2 = self.skip_connection_conv_2(block_2_output)
        # print("Shape of input_to_skip_2 is ", input_to_skip_2.size())



        # operations for skip connection 3
        input_to_skip_3 = self.skip_connection_conv_3(block_3_output)
        # print("Shape of input_to_skip_3 is ", input_to_skip_3.size())



        # Operations for skip connection 4
        input_to_skip_4 = self.skip_connection_conv_4(block_4_output)
        # print("Shape of input_to_skip_4 is ", input_to_skip_4.size())
        input_to_skip_4 = self.upsample(input_to_skip_4)
        #####################################################################################
        # concatenting outputs from all CONV BLOCKS
        output = torch.cat([input_to_skip_1, input_to_skip_2, input_to_skip_3, input_to_skip_4], dim=1)
        # print("Shape of concatenated output is ", output.size())



        # Convolution on CNN features before inputting to LSTM
        output = self.conv_from_skip_connection_to_rnn(output)
        # print("Shape of concatenated output after layer 5 is ", output.size())


        edge_logits = self.e_conv(output)
        # print("Shape of edge_logits is ", edge_logits.size())

        vertex_logits = self.v_conv(output)
        # print("Shape of vertex_logits is ", vertex_logits.size())

        edge_logits = nn.Softmax2d()(edge_logits)
        # print("Shape of edge_logits after softmax is ", edge_logits.size())

        vertex_logits = nn.Softmax2d()(vertex_logits)
        # print("Shape of vertex_logits after softmax is ", vertex_logits.size())

        return edge_logits, vertex_logits
