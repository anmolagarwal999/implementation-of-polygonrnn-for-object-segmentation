from torch.utils.data.dataset import Dataset
from torchvision import transforms
import json

class FirstVDataset(Dataset):
    def __init__(self):
        super().__init__()

    def __getitem__(self, index):
        with open(f"first_v_dataset/{index}.json", 'r') as f:
            data = json.load(f)
        
        return data
