import torch
from torch import nn
from torch.autograd import Variable




class ConvLSTM(nn.Module):

    def __init__(self, cnn_embedding_size, channels_in_input, hidden_dim, kernel_size,
                 num_layers,
                 batch_first=False, bias=True, return_all_layers=False):
        super(ConvLSTM, self).__init__()

        #########################################################################
        ############## MINOR FORMAT CHECKING STUFF ########################
        self._check_kernel_size_consistency(kernel_size)

        # Make sure that both `kernel_size` and `hidden_dim` are lists
        # having len == num_layers
        kernel_size = self._extend_for_multilayer(kernel_size, num_layers)
        hidden_dim = self._extend_for_multilayer(hidden_dim, num_layers)
        if not len(kernel_size) == len(hidden_dim) == num_layers:
            raise ValueError('Inconsistent list length.')
        ###################################################################

        ########################################
        self.height, self.width = cnn_embedding_size
        assert(self.height==28 and self.width==28)
        #######################################

        self.channels_in_input = channels_in_input
        assert(self.channels_in_input==131)

        #################################
        # list containing hidden dimensions expected in different lstm layers
        self.hidden_dim = hidden_dim
        ###############################

        self.kernel_size = kernel_size
        assert(kernel_size[0]==(3,3) and kernel_size[1]==(3,3))


        self.num_layers = num_layers


        self.batch_first = batch_first
        assert(batch_first==True)


        self.bias = bias
        assert(self.bias)


        self.return_all_layers = return_all_layers


        cell_list = []
        for i in range(0, self.num_layers):
            # curr_input_dim is number of channels in x_t
            cur_input_channels = self.channels_in_input if i == 0 else self.hidden_dim[
                i - 1]

            cell_list.append(ConvLSTMCell(input_size=(self.height, self.width),
                                          channels_in_input=cur_input_channels,
                                          hidden_dim=self.hidden_dim[i],
                                          kernel_size=self.kernel_size[i],
                                          bias=self.bias))

        self.cell_list = nn.ModuleList(cell_list)

    def forward(self, input_tensor, hidden_state=None):
        """

        Parameters
        ----------
        input_tensor: todo
            5-D Tensor either of shape (t, b, c, h, w) or (b, t, c, h, w)
        hidden_state: todo
            None. todo implement stateful

        Returns
        -------
        last_state_list, layer_output
        """
        if not self.batch_first:
            # (t, b, c, h, w) -> (b, t, c, h, w)
            input_tensor.permute(1, 0, 2, 3, 4)

        # print("Shape of input tensor in ConvLSTM is " ,type(input_tensor))
        # [2, 58, 131, 28, 28]
        # print("Shape of input tensor in ConvLSTM is " ,input_tensor.size())

       
        # number of hidden states will depend on batch size
        hidden_state = self._init_hidden(batch_size=input_tensor.size(0))
        # print("[Expect zero ] :Shape of hidden state in CONVLSTM is ", type(hidden_state))
        '''For each datapoint, a separate hidden state and context state are created'''
        # print("[Expect zero ] :Shape of hidden state in CONVLSTM is ", len(hidden_state))
        # print("[Expect zero ] :Shape of hidden state WITHIN in CONVLSTM[0][0] is ", hidden_state[0][0].size())
        # print("[Expect zero ] :Shape of hidden state WITHIN in CONVLSTM[0][1] is ", hidden_state[0][1].size())

        layer_output_list = []
        last_state_list = []

        # recovering max length of vertices
        # [2, 58, 131, 28, 28]
        seq_len = input_tensor.size(1)

        # input tensor
        cur_layer_input = input_tensor

        for layer_idx in range(self.num_layers):
            # print("LAYER IDX IS ", layer_idx)
            # personal hidden and context state for each data point
            # [2, 32, 28, 28]
            h, c = hidden_state[layer_idx]
            output_inner = []

            ##########################################
            for t in range(seq_len):
                # print("timestep is ", t)
                #                 print(cur_layer_input.shape)
                # [2, 32, 28, 28]
                '''
                For each data point, h contains (32,28,28) element which pertains to the hidden state which should be used for this element for this timestep
                '''
                h, c = self.cell_list[layer_idx](
                    input_tensor=cur_layer_input[:, t, :, :, :], # (2, 131, 28,28)
                    cur_state=[h, c])
                output_inner.append(h)
            ##################################
            # len of output ineer will be 58
            # each element will be 2, 32, 28, 28

            # for second LSTM layer, output will be the hidden states in the corresponding timesteps of previous layers
            '''
            For each data point, layer_output contains the hidden state for each time step from previous lSTM layer
            So, basically, the input for each data point is (max seq, 32, 28,28)
            '''
            layer_output = torch.stack(output_inner, dim=1)
            # print("laye_Output size is ",layer_output.size())
            cur_layer_input = layer_output

            layer_output_list.append(layer_output)
            last_state_list.append([h, c])

        if not self.return_all_layers:
            layer_output_list = layer_output_list[-1:]
            last_state_list = last_state_list[-1:]


        # [2, 58, 32, 28, 28],
        # [2, 58, 8, 28, 28]
        '''
        ith element of the layer output list is :
        all the timeseries outputs for each data point in the ith LSTM layer
        '''
        return layer_output_list, last_state_list

    def _init_hidden(self, batch_size):
        init_states = []
        for i in range(self.num_layers):
            init_states.append(self.cell_list[i].init_hidden_and_context(batch_size))
        return init_states

    @staticmethod
    def _check_kernel_size_consistency(kernel_size):
        if not (isinstance(kernel_size, tuple) or
                (isinstance(kernel_size, list) and all(
                    [isinstance(elem, tuple) for elem in kernel_size]))):
            raise ValueError('`kernel_size` must be tuple or list of tuples')

    @staticmethod
    def _extend_for_multilayer(param, num_layers):
        if not isinstance(param, list):
            param = [param] * num_layers
        return param


class ConvLSTMCell(nn.Module):

    def __init__(self, input_size, channels_in_input, hidden_dim, kernel_size, bias):
        """
        Initialize ConvLSTM cell.

        Parameters
        ----------
        input_size: (int, int)
            Height and width of input tensor as (height, width).
        channels_in_input: int
            Number of channels of input tensor.
        hidden_dim: int
            Number of channels of hidden state.
        kernel_size: (int, int)
            Size of the convolutional kernel.
        bias: bool
            Whether or not to add the bias.
        """

        super(ConvLSTMCell, self).__init__()

        # put assert
        self.height, self.width = input_size

        # put assert
        self.channels_in_input = channels_in_input

        # put assert : authors say 16 channels
        
        self.hidden_dim = hidden_dim


        self.kernel_size = kernel_size
        assert(kernel_size==(3,3))


        self.padding = kernel_size[0] // 2, kernel_size[1] // 2
        assert(self.padding==(1,1))

        self.bias = bias
        assert(self.bias==True)


        # hidden h_{t-1} will be concatenated with input "x_t"
        self.conv = nn.Conv2d(in_channels=self.channels_in_input + self.hidden_dim,
                              out_channels=4 * self.hidden_dim, # 1 for each pf the gates
                              kernel_size=self.kernel_size,
                              padding=self.padding,
                              bias=self.bias)

    def forward(self, input_tensor, cur_state):

        '''
        input tensor: 28 * 28 * 131 for 1st lstm layer
                    28 * 28 * {hidden dimension of 1st lstm} for the 2nd lstm layer
        '''

        # [2, 131, 28, 28]
        # print("Size of input tensor within batch cell is ", input_tensor.size())
        # get the current context
        h_cur, c_cur = cur_state
        # [2, 32, 28, 28], [2, 32, 28, 28]
        # print("size of h_cur within CONV LSTM cell IS ", h_cur.size())

        # [2, 163, 28, 28]
        combined = torch.cat([input_tensor, h_cur],
                             dim=1)  # concatenate along channel axis
        # print("dimensions of combined is ", combined.size())

        combined_conv = self.conv(combined)
        # print("dimensions of combined_conv is ", combined_conv.size())

        cc_i, cc_f, cc_o, cc_g = torch.split(combined_conv, self.hidden_dim,
                                             dim=1)

        # print("size of cc_i is ", cc_i.size())
        # print("size of cc_f is ", cc_f.size())
        # print("size of cc_o is ", cc_o.size())
        # print("size of cc_g is ", cc_g.size())
        

        # find output of all the gates
        i = torch.sigmoid(cc_i)
        f = torch.sigmoid(cc_f)
        o = torch.sigmoid(cc_o)
        g = torch.tanh(cc_g)

        # print("size of i is ", i.size())
        # print("size of f is ", f.size())
        # print("size of o is ", o.size())
        # print("size of g is ", g.size())


        c_next = f * c_cur + i * g
        h_next = o * torch.tanh(c_next)

        # print("size of c_next is ", c_next.size())
        # print("size of h_next is ", h_next.size())


        return h_next, c_next

    def init_hidden_and_context(self, batch_size):

        # return (Variable(torch.zeros(batch_size, self.hidden_dim, self.height,
        #                              self.width)),
        #         Variable(torch.zeros(batch_size, self.hidden_dim, self.height,
        #                              self.width)))
        return (Variable(torch.zeros(batch_size, self.hidden_dim, self.height,
                                     self.width)).cuda(),
                Variable(torch.zeros(batch_size, self.hidden_dim, self.height,
                                     self.width)).cuda())
