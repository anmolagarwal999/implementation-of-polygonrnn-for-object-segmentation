import argparse

import torch.utils.data
from torch import nn
from torch import optim
from torch.autograd import Variable

from first_v_model import FirstV_Model
from firstv_dataset import FirstVDataset
#from data import load_data
from model import PolygonRnn
#from test import test
import wget
import torch.nn.functional as F


import os
import sys
import json

#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from pathlib import Path

import torch
import torch.nn.init
import torch.utils.model_zoo as model_zoo
import wget
from torch import nn
from tensorboardX import SummaryWriter

from utils.convlstm import ConvLSTM


from dataset_creation_utils import *

def get_loss(e_pred, v_pred, e_gt, v_gt):
    e_loss = F.binary_cross_entropy(e_pred, e_gt)
    v_loss = F.binary_cross_entropy(v_pred, v_gt)
    return e_loss+ v_loss

def main():

    dataloader = torch.utils.data.DataLoader(FirstVDataset(), batch_size=8,
                                                shuffle=True, drop_last=False)
    epochs = 50

    model = FirstV_Model()

    optimizer = optim.Adam(model.parameters(), lr=0.001)
    scheduler = optim.lr_scheduler.MultiStepLR(optimizer,
                                               milestones=[34000, 80000],
                                               gamma=0.1)


    for curr_epoch in epochs:
        print("curr_epoch is ", curr_epoch)
        loss_epoch = 0
        for step, data in enumerate(dataloader):
            print(f"step is {step}")

            img = Variable(data["img"].type(torch.cuda.FloatTensor))
            v_gt = Variable(data["vertice_logits_gt"].type(torch.cuda.FloatTensor))
            e_gt = Variable(data["edge_logits_gt"].type(torch.cuda.FloatTensor))

            optimizer.zero_grad()
            e_pred, v_pred = model(img)
            loss = get_loss(e_pred, v_pred, e_gt, v_gt)
            loss.backward()
            optimizer.step()
            scheduler.step()

            loss_epoch += loss.item()

        print(f"loss is {loss_epoch}")
        torch.save(model.state_dict(), f"./first_v_cp/firstv_model_epoch_{curr_epoch}.pth")



