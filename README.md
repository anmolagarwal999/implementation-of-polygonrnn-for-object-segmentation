# Computer Vision Project

**Paper Name**: Annotating Object Instances with a Polygon-RNN
**Project ID**: 36
**Team Name**: Team Visionaries
**Team members**: Anmol Agarwal, Vedansh Mittal, Ashwin Mittal
**Mentor Name**: Samartha, Avinash Prabhu

Implemented as a part of the Computer Vision course at IIIT H.


#### NOTE:
For a detailed overview of our work, please see `overview_slides.pdf` or our slides submission on Moodle.
This README only contains details regarding the file structure, and links to `weights of the various models`. Please see the latter slides of the PPT for our results on various objects within and NOT within the CityScapes dataset.

### Folder structure
```
.
├── 01 dataset_creation.ipynb : to create  processed datasets from raw dataset
├── config_stuff
│   └── dataset_creation.json : holds various paths and configuration values as JSON
├
├── dataset_creation_utils.py: helper dataset creation
├── firstv_dataset.py : dataset for first vertex prediction model: assumes that preprocessed dataset is already stored. Not stored in this repo due to space issues. For the folder containing the preprocessed data, refer to the OneDrive links below.
├── first_v_model.py : uses same model as model architecture as model.py
├── logs_dir : holds logs which can be loaded into tensorboard
├── model.py : main model (has code for both forward pass during training and the code for human intervention. For testing code without human intervention, use the HUMAN INTERVENTION TESTING CODE but either put the THRESHOLD very high OR keeep num_corrections allowed to be zero)
├── run.py: main training code
├── train_fv.py : training code for first vertex model
└── utils : stores misc. utils
    ├── convlstm.py : pytorch implementation for Conv LSTMs commonly used. We made minor changes to this to suit our needs.
    ├── __init__.py
├── plots_and_stuff: code uses the downloaded vertices of different objects from the server. It uses the vertices stored in JSON format to make the 
│                      polygon as indicate by the vertices predicted by the model. It also put all objects beloning to the same scene in the same   scene and saves the image.
```


### Some results on images taken in and around IIIT-H:
![](https://i.imgur.com/95rf21Y.jpg)
![](https://i.imgur.com/WjUzf0l.png)
![](https://i.imgur.com/0UveTom.jpg)
![](https://i.imgur.com/Bi6YC7Y.jpg)



### Various links
* Link to weights for model trained on all categories:: https://iiitaphyd-my.sharepoint.com/:f:/g/personal/anmol_agarwal_students_iiit_ac_in/EmMyuvK2pCVEozAUlBCvd1ABCOtpYgEEzgrb5e-Sak1pNw?e=1paDiT 
* Folder storing results and config files: https://iiitaphyd-my.sharepoint.com/:f:/g/personal/anmol_agarwal_students_iiit_ac_in/EiiGpSJwYMBPlUzRCFDEvOgBH8yH4C95uT_DOKuMo4KWOg?e=SBBTYs



#### Frontend Demo Picture
![](https://i.imgur.com/GWmxfd2.png)


#### Feedback by Project mentors :smiley:
![](https://i.imgur.com/pB9NZzG.png)

